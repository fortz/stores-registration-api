<?php
require 'vendor/autoload.php';

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

$app = new \Slim\App;

function requestHasAllParameters($body) {

    return isset($body->store_name) &&
        isset($body->latitude) &&
        isset($body->longitude) &&
        isset($body->street) &&
        isset($body->notes);
}


$app->get('/stores', function(ServerRequestInterface $request, ResponseInterface $response) {
    $db = new mysqli("mysql.hostinger.com", "u622434154_fortz", "*f0rtz.d3v*", "u622434154_store");

    $query =" SELECT * FROM Stores ";

    $jsonResponse = array();
    if ($result = $db->query($query)) {

        while($row = $result->fetch_object()){
            array_push($jsonResponse, $row);
        }

        $response->getBody()->write(json_encode(array(
            "status" => "success",
            "message" => "stores retrieved successfully",
            "stores" => $jsonResponse
        )));
    } else {
        $response->getBody()->write(json_encode(array(
            "status" => "error",
            "message" => "error executing query",
            "stores" => $jsonResponse
        )));
    }

    return $response;
});

$app->delete('/store/{storeID}', function($request, $response, $args) {
    $db = new mysqli("mysql.hostinger.com", "u622434154_fortz", "*f0rtz.d3v*", "u622434154_store");

    $storeID = $args["storeID"];
    $stmt = $db->prepare("DELETE FROM Stores WHERE id = ?");
    $stmt->bind_param('i', $storeID);
    $success= $stmt->execute();
    $stmt->close();

    if ($success) {
        $response->getBody()->write(json_encode(array(
            "status" => "success",
            "message" => "store " . $storeID . " deleted successfully",
        )));
    } else {
        $response->getBody()->write(json_encode(array(
            "status" => "error",
            "message" => "error executing query",
        )));
    }

    return $response;
});


$app->post('/store', function(ServerRequestInterface $request, ResponseInterface $response) {

    $body = json_decode(file_get_contents('php://input'));
    if(!requestHasAllParameters($body)) {
        $response->getBody()->write(json_encode(array(
            "status" => "error",
            "message" => "missing parameter:\nstore_name: " . $body->store_name . "\nlatitude: " . $body->latitude . "\nlongitude: " . $body->longitude . "\nstreet: " . $body->street . "\nnotes: " . $body->notes
        )));

        return $response;
    }

    $db = new mysqli("mysql.hostinger.com", "u622434154_fortz", "*f0rtz.d3v*", "u622434154_store");

    $query =" INSERT INTO Stores (store_name, latitude, longitude, street, notes) VALUES (?, ?, ?, ?, ?) ";
    if(!$stmt = $db->prepare($query)) {
        echo "Prepare failed: (" . $db->errno . ") " . $db->error;
    }
    $stmt->bind_param('sddss', $body->store_name, $body->latitude, $body->longitude, $body->street, $body->notes);
    if($stmt->execute()) {
        $response->getBody()->write(json_encode(array(
            "status" => "success",
            "message" => "store inserted"
        )));
    } else {
        $response->getBody()->write(json_encode(array(
            "status" => "error",
            "message" => "error executing query"
        )));
    }

    return $response;
});

$app->get('/', function() {
    echo "WELCOME TO THE MATRIX.";
});

$app->run();
