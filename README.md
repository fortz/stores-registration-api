# README #

Simple API Rest service for registering stores into DB.
### API contract (base URL: fortz.esy.es) ###

* POST /store
* DELETE /store/{storeID}
* GET /stores



### POST /store ###
Request for register a new shop.

*Parameters:*

* **shop_name**: Name of the store
* **latitude** and **longitude**: Coordinates of the store
* **address**: Human readable string of store's address
* **notes**: Some notes about the store

*Response:*
```
{
  "status": "success",
  "message": "store inserted"
}
```
* **status**: request execution status (**success** / **error**)
* **message**: additional status information request execution status


### DELETE /store/{storeID} ###
Request for remove a registered shop with ID *storeID*.

*Parameters:*

This request doesn't take parameters.


*Response:*
```
{
  "status": "success",
  "message": "store deleted"
}
```
* **status**: request execution status (**success** / **error**)
* **message**: additional status information request execution status


### GET /stores ###
Request for retrieve all registered stores.

*Response:*
```
{
  "status": "success",
  "message": "stores retrieved successfully",
  "stores": [
    {
      "id": "STORE ID",
      "store_name": "STORE NAME",
      "latitude": "STORE'S LOCATION LATITUDE",
      "longitude": "STORE'S LOCATION LONGITUDE",
      "address": "STORE ADDRESS",
      "notes": "STORE NOTES"
    },
    ...
  ]
}
```
* **status**: request execution status (**success** / **error**)
* **message**: additional status information request execution status
* **stores**: array of stores